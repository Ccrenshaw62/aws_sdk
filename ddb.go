package awssdk

import (
	"strings"
	"time"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodbstreams"
)

// DDBTable xxx
type DDBTable struct {
	ddbTable  string
	config    Config
	streamArn string
	ddbClient	*dynamodb.DynamoDB
	strClient   *dynamodbstreams.DynamoDBStreams
	shards map[string]*ddbShard
	RowUpdates chan map[string]string
	Errors     chan error
	Done       chan bool
}

type ddbShard struct {
	shardID   string
	shardIT   string
	nextSeq   string
	lastSeq   string
	doneSeq   string
	inAWS     bool
}

// NewDDBTable xxx
func NewDDBTable(config Config, table string) (*DDBTable, error) {
	if session, err := NewSession(config.Region, config.Key, config.Secret, config.Token, config.Credfile, config.Profile); err == nil {
		ddb := dynamodb.New(session)
		return &DDBTable{
			ddbClient: ddb,
			ddbTable: table,
			config: config,
			shards: make(map[string]*ddbShard),
			RowUpdates: make(chan map[string]string),
			Errors: make(chan error),
			Done: make(chan bool),
		}, nil
	} else {
		return nil, err
	}
}

// Run xxx
func (dt *DDBTable) run() (stop chan bool) {
	stop = make(chan bool)
	go func() {
		for {
			select {
			case <-time.After(time.Duration(1) * time.Second):
				dt.readStream()
			case <-stop:
				return
			}
		}
	}()
	return
}

// ReadTable reads all data from the table, and sets markers at the ends of the shard streams to support reading ongoing updates.
func (dt *DDBTable) ReadTable() ([]map[string]string, chan bool, error) {
	// Steps to start reading a table
	// 0. Get the ARN for the table.
	// 1. Get the set of shards for the table.
	// 2. Mark the current ends of each shard.
	// 3. Completely read the content of the table.
	// 4. Start reading the table shards (streams) from their marks.
	items := make([]map[string]string, 5000)

	if err := dt.refreshShards(); err != nil {
		return items, nil, err
	}

	// Marks the ends of the shards. Don't retrieve data here, just mark the ends.
	for _, shard := range dt.shards {
		shard.readShard(dt.strClient, dt.streamArn, true)
	}

	retry := 50

	var results *dynamodb.ScanOutput
	var err error

	keepReading := true
	for keepReading {
		keepReading = false

		scanInput := &dynamodb.ScanInput{
			TableName: aws.String(dt.ddbTable),
		}
		if results != nil {
			scanInput.ExclusiveStartKey = results.LastEvaluatedKey
		}
		if results, err = dt.ddbClient.Scan(scanInput); err == nil {
			for _, ri := range results.Items {
				item := make(map[string]string)
				items = append(items, item)
				for key, value := range ri {
					if !strings.HasPrefix(key, "aws:rep") {
						item[key] = attrValString(value)
					}
				}
			}
			if len(results.LastEvaluatedKey) > 0 {
				keepReading = true
			}
		} else {
			if aerr, ok := err.(awserr.Error); ok {
				switch aerr.Code() {
				case dynamodbstreams.ErrCodeResourceNotFoundException:
					// Non-existent stream. This shard reader is done.
					return items, nil, err

				case dynamodbstreams.ErrCodeInternalServerError:
					// Not sure if retrying will help. Let's back off and retry once.
					fallthrough
				case dynamodbstreams.ErrCodeLimitExceededException:
					fallthrough
				case dynamodb.ErrCodeProvisionedThroughputExceededException:
					if retry >= 30*1000 {
						return items, nil, err
					}
					time.Sleep(time.Duration(retry) * time.Millisecond)
					retry *= 2
					keepReading = true
				default:
					return items, nil, err
				}
			} else {
				return items, nil, err
			}
		}
	}
	stop := dt.run()
	return items, stop, nil
}

func (dt *DDBTable) readStream() error {
	if err := dt.refreshShards(); err != nil {
		return err
	}
	for _, shard := range dt.shards {
		rows, err := shard.readShard(dt.strClient, dt.streamArn, false)
		for _, row := range rows {
			dt.RowUpdates <- row
		}
		if err != nil {
			delete(dt.shards, shard.shardID)
		}
	}
	return nil
}

func (dt *DDBTable) refreshTable() error {
	if ddbDesc, err := dt.ddbClient.DescribeTable(&dynamodb.DescribeTableInput{TableName: aws.String(dt.ddbTable)}); err == nil {
		dt.streamArn = *ddbDesc.Table.LatestStreamArn
		if session, err := NewSession(dt.config.Region, dt.config.Key, dt.config.Secret, dt.config.Token, dt.config.Credfile, dt.config.Profile); err == nil {
			dt.strClient = dynamodbstreams.New(session)
			return nil
		} else {
			return err
		}
	} else {
		return err
	}
}

func (dt *DDBTable) refreshShards() error {
	if err := dt.refreshTable(); err != nil {
		return err
	}
	for _, shard := range dt.shards {
		shard.inAWS = false
	}
	doneShards := make(map[string]bool)
	if strDesc, err := dt.strClient.DescribeStream(&dynamodbstreams.DescribeStreamInput{StreamArn: aws.String(dt.streamArn)}); err == nil {
		// Look at each shard in AWS
		for _, shard := range strDesc.StreamDescription.Shards {
			var ourShard *ddbShard
			var found bool
			// If we don't have this shard yet, add it now.
			if ourShard, found = dt.shards[*shard.ShardId]; !found {
				ourShard = &ddbShard{shardID: *shard.ShardId}
				dt.shards[*shard.ShardId] = ourShard
			}
			ourShard.inAWS = true

			// If the shard has an ending sequence number, store it.
			if shard.SequenceNumberRange != nil && shard.SequenceNumberRange.EndingSequenceNumber != nil && *shard.SequenceNumberRange.EndingSequenceNumber != "" {
				ourShard.doneSeq = *shard.SequenceNumberRange.EndingSequenceNumber
			}
		}
		// Add shards to the list of local shards to delete.
		for shardID, ourShard := range dt.shards {
			// Shards not in AWS get deleted.
			if ourShard.inAWS == false {
				doneShards[shardID] = true
			}
			// Shards that we have completed reading get deleted.
			if ourShard.doneSeq != "" && ourShard.lastSeq == ourShard.doneSeq {
				doneShards[shardID] = true
			}
		}
		// Delete shards in the delete list.
		for shardID := range doneShards {
			delete(dt.shards, shardID)
		}
	}
	return nil
}

func (ds *ddbShard) updateIter(strClient *dynamodbstreams.DynamoDBStreams, streamArn string, fromStart bool) error {
	for {
		input := &dynamodbstreams.GetShardIteratorInput{
			ShardId:   aws.String(ds.shardID),
			StreamArn: aws.String(streamArn),
		}
		if fromStart {
			input.ShardIteratorType = aws.String(dynamodbstreams.ShardIteratorTypeTrimHorizon)
		} else if ds.nextSeq != "" {
			input.ShardIteratorType = aws.String(dynamodbstreams.ShardIteratorTypeAtSequenceNumber)
			input.SequenceNumber = aws.String(ds.nextSeq)
		} else if ds.lastSeq != "" {
			input.ShardIteratorType = aws.String(dynamodbstreams.ShardIteratorTypeAfterSequenceNumber)
			input.SequenceNumber = aws.String(ds.lastSeq)
		} else {
			input.ShardIteratorType = aws.String(dynamodbstreams.ShardIteratorTypeLatest)
		}
		if shardIterOut, err := strClient.GetShardIterator(input); err != nil {
			if aerr, ok := err.(awserr.Error); ok {
				switch aerr.Code() {
				case dynamodbstreams.ErrCodeTrimmedDataAccessException:
					// Iterator goes back prior to 24 hours (or whatever the retention period is for this stream).
					// Get a new iterator for as far back as we can go - best we can do.
					fromStart = true

				case dynamodbstreams.ErrCodeInternalServerError:
					fallthrough

				case dynamodbstreams.ErrCodeResourceNotFoundException:
					// Non-existent stream. This shard reader is done.
					fallthrough

				default:
					return err
				}
			}
		} else {
			ds.shardIT = *shardIterOut.ShardIterator
			return nil
		}
	}
}

func (ds *ddbShard) readShard(strClient *dynamodbstreams.DynamoDBStreams, streamArn string, mark bool) ([]map[string]string, error) {
	items := make([]map[string]string, 0, 1000)
	limit := int64(1000)
	sleepDelay := 500
	fromStart := false
	if mark {
		limit = 1
	}
	for {
		if ds.shardIT == "" {
			if err := ds.updateIter(strClient, streamArn, fromStart); err != nil {
				return items, err
			}
			fromStart = false
			continue
		}
		if getRecordOut, err := strClient.GetRecords(&dynamodbstreams.GetRecordsInput{
			ShardIterator: aws.String(ds.shardIT),
			Limit:         aws.Int64(limit),
		}); err == nil {
			for _, record := range getRecordOut.Records {
				if mark {
					ds.nextSeq = *record.Dynamodb.SequenceNumber
					ds.shardIT = ""
					return items, err
				} else {
					ds.lastSeq = *record.Dynamodb.SequenceNumber
				}
				eventName := *record.EventName
				tableName := *record.EventSource
				item := make(map[string]string)
				item["_event"] = eventName
				item["_table"] = tableName
				var image map[string]*dynamodb.AttributeValue
				if eventName == "INSERT" || eventName == "MODIFY" {
					image = record.Dynamodb.NewImage
				} else {
					image = record.Dynamodb.OldImage
				}
				for attrName, attrVal := range image {
					if !strings.HasPrefix(attrName, "aws:rep") {
						item[attrName] = attrValString(attrVal)
					}
				}
				items = append(items, item)

				if getRecordOut.NextShardIterator != nil && *getRecordOut.NextShardIterator != "" {
					ds.shardIT = *getRecordOut.NextShardIterator
				}
			}
			return items, nil
		} else {
			ds.shardIT = ""
			if aerr, ok := err.(awserr.Error); ok {
				switch aerr.Code() {
				case dynamodbstreams.ErrCodeInternalServerError:
					fallthrough

				case dynamodbstreams.ErrCodeResourceNotFoundException:
					// Non-existent stream. This shard reader is done.
					return items, err

				case dynamodbstreams.ErrCodeLimitExceededException:
					if sleepDelay >= 30*1000 {
						return items, err
					}
					time.Sleep(time.Duration(sleepDelay) * time.Millisecond)
					sleepDelay *= 2

				case dynamodbstreams.ErrCodeExpiredIteratorException:
					// Iterator expired. Shard still good. Get new iterator based on where we left off. (iterators expire after 15 minutes - according to doc)
					// If we have a lastSeq number, it means we have a starting point in the shard. If no lastSeq, we haven't read anything yet. Start at beginning.
					
				case dynamodbstreams.ErrCodeTrimmedDataAccessException:
					// Iterator goes back prior to 24 hours (or whatever the retention period is for this stream).
					// Get a new iterator for as far back as we can go - best we can do.
					fromStart = true

				default:
					// Should never get here.
					return items, err
				}
			} else {
				if sleepDelay >= 30*1000 {
					return items, err
				}
				time.Sleep(time.Duration(sleepDelay) * time.Millisecond)
				sleepDelay *= 2
			}
		}
	}
}

func attrValString(attrVal *dynamodb.AttributeValue) string {
	if attrVal.S != nil {
		return *attrVal.S
	} else if attrVal.N != nil {
		return *attrVal.N
	} else {
		return ""
	}
}