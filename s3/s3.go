// Package s3 is a simple package that provides access to AWS S3 by wrapping the more complex AWS S3 SDK.
package s3

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	awssdk "bitbucket.org/Ccrenshaw62/aws_sdk"
	"os"
	"time"
	"strings"
	"log"
)

// Entry xxx
type Entry struct {
	Region   string
	Bucket   string
	Key      string
	Dir      string
	File     string
	Modified time.Time
}

// Upload xxx
func Upload(folder, filename, region, bucket, prefix string) (localTime time.Time, localSize int64, remoteTime time.Time, remoteSize int64,  err error) {
	if awsSession, err := awssdk.NewSession(region, "", "", "", ""); err == nil {
		s3Session := s3.New(awsSession)
		pathname := folder + "/" + filename
		keyname  := prefix + "/" + filename
		if file, err := os.Open(pathname); err == nil {
			defer file.Close()
			uploader := s3manager.NewUploaderWithClient(s3Session)
			if _, err := uploader.Upload(&s3manager.UploadInput{
				Bucket: &bucket,
				Key:    &keyname,
				Body:   file,
			}); err == nil {
				if fileInfo, err := file.Stat(); err == nil {
					localTime = fileInfo.ModTime()
					localSize = fileInfo.Size()
				}
				if modified, size, err := remoteObjectProperties(s3Session, bucket, keyname); err == nil {
					remoteTime = modified
					remoteSize = size
				}
			}
		}
	}
	return
}

// Download xxx
func Download(folder, filename, region, bucket, prefix string) (localTime time.Time, localSize int64, remoteTime time.Time, remoteSize int64,  err error) {
	if awsSession, err := awssdk.NewSession(region, "", "", "", ""); err == nil {
		s3Session := s3.New(awsSession)
		pathname := folder + "/" + filename
		keyname  := prefix + "/" + filename
		os.MkdirAll(folder, 0777)
		if file, err := os.Create(pathname); err == nil {
			defer file.Close()
			downloader := s3manager.NewDownloaderWithClient(s3Session)
			if _, err := downloader.Download(file, &s3.GetObjectInput{
				Bucket: &bucket,
				Key:    &keyname,
			}); err == nil {
				if fileInfo, err := file.Stat(); err == nil {
					localTime = fileInfo.ModTime()
					localSize = fileInfo.Size()
				}
				if modified, size, err := remoteObjectProperties(s3Session, bucket, keyname); err == nil {
					remoteTime = modified
					remoteSize = size
				}
			}
		} else {
			log.Printf("s3.Download(%s, %s, %s, %s): failed - %s", folder, filename, region, bucket, err)
		}
	}
	return
}

// List xxx
func List(region, bucket, rawPrefix string) []Entry {
	var prefix, delim string
	if rawPrefix == "/" || rawPrefix == "" {
		prefix = ""
		delim = "/"
	} else if strings.HasPrefix(rawPrefix, "/") {
		prefix = rawPrefix[1:]
		delim = ""
	} else {
		prefix = rawPrefix
		delim = ""
	}
	if len(prefix) > 0 && !strings.HasSuffix(prefix, "/") {
		prefix = prefix + "/"
	}
	entries := make([]Entry, 0, 50)
	if awsSession, err := awssdk.NewSession(region, "", "", "", ""); err == nil {
		s3Session := s3.New(awsSession)
		readNext  := true
		nextMarker:= ""
		loi := &s3.ListObjectsInput{
			Bucket:	   aws.String(bucket),
			Prefix:    aws.String(prefix),
			Delimiter: aws.String(delim),
		}
		for readNext {
			if nextMarker != "" {
				loi.Marker = aws.String(nextMarker)
			}
			var lastKey string
			if resp, err := s3Session.ListObjects(loi); err == nil {
				for _, item := range resp.Contents {
					entry := Entry{}
					entry.Key = *item.Key
					lastKey = entry.Key
					// Remove the directory path up to the filename itself.
					if index := strings.LastIndex(entry.Key, "/"); index > -1 {
						entry.Dir  = entry.Key[:index-1]
						entry.File = entry.Key[index+1:]
					} else {
						entry.Dir  = entry.Key
						entry.File = entry.Key
					}
					entry.Bucket = bucket
					entry.Region = region
					entry.Modified = *item.LastModified
	
					entries = append(entries, entry)
				}
				if *resp.IsTruncated {
					if resp.NextMarker != nil && len(*resp.NextMarker) > 0 {
						nextMarker = *resp.NextMarker
					} else {
						nextMarker = lastKey
					}
				} else {
					readNext = false
				}
			}
		}
	}
	return entries
}

func remoteObjectProperties(s3Client *s3.S3, bucket, key string) (modified time.Time, size int64, err error) {
	if remoteObject, err := s3Client.GetObject(&s3.GetObjectInput{
		Bucket: &bucket,
		Key:    &key,
	}); err == nil {
		modified = *remoteObject.LastModified
		size = *remoteObject.ContentLength
	}
	return
}

// CreateBucket creates a bucket - if existsOk is true, then a failure to create due to an existing bucket will return success.
func CreateBucket(region, bucket string, existsOk bool) error {
	if awsSession, err := awssdk.NewSession(region, "", "", "", ""); err == nil {
		s3Session := s3.New(awsSession)
		if _, err := s3Session.CreateBucket(&s3.CreateBucketInput{
			Bucket: aws.String(bucket),
			CreateBucketConfiguration: &s3.CreateBucketConfiguration{
				LocationConstraint: aws.String(region),
			},
		}); err != nil {
			// We say it's okay if we fail because the bucket already exists (existsOk).
			if existsOk && (
				strings.HasPrefix(err.Error(),s3.ErrCodeBucketAlreadyExists) || strings.HasPrefix(err.Error(),s3.ErrCodeBucketAlreadyOwnedByYou)) {
				return nil
			}
			return err
		}
	} else {
		return err
	}
	return nil
}

// DeleteBucket deletes the bucket.
func DeleteBucket(region, bucket string) error {
	if awsSession, err := awssdk.NewSession(region, "", "", "", ""); err == nil {
		s3Session := s3.New(awsSession)
		if _, err := s3Session.DeleteBucket(&s3.DeleteBucketInput{
			Bucket: aws.String(bucket),
		}); err != nil {
			return err
		}
	} else {
		return err
	}
	return nil
}

// DeleteObject deletes the object.
func DeleteObject(region, bucket, key string) error {
	if awsSession, err := awssdk.NewSession(region, "", "", "", ""); err == nil {
		s3Session := s3.New(awsSession)
		if _, err := s3Session.DeleteObject(&s3.DeleteObjectInput{
			Bucket: aws.String(bucket),
			Key:    aws.String(key),
		}); err != nil {
			return err
		}
	} else {
		return err
	}
	return nil
}
