package ddb

import (
	awssdk "bitbucket.org/Ccrenshaw62/aws_sdk"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodbstreams"
	"time"
)

// StreamReader xxx
type StreamReader struct {
	client       *dynamodbstreams.DynamoDBStreams
	streamArn    string
	shardReaders []*ShardReader
}

// ShardReader xxx
type ShardReader struct {
	shardID string
	shardIt string
	lastSeq string
	stop    chan bool
	inDDB   bool
}

// Event xxx
type Event struct {
	operation string
	table     string
	data      map[string]string
}

// Connect xxx
func Connect(c *awssdk.Config, ddbConn *dynamodb.DynamoDB) (*dynamodb.DynamoDB, error) {
	if ddbConn == nil {
		if session, err := awssdk.NewSession(c.Region, c.Key, c.Secret, c.Credfile, c.Profile); err == nil {
			ddb := dynamodb.New(session)
			return ddb, nil
		} else {
			return nil, err
		}
	}
	return ddbConn, nil
}

// Insert xxx
func Insert(ddbConn *dynamodb.DynamoDB, ddbTable string, data map[string]string) error {
	return nil
}

// Update xxx
func Update(ddbConn *dynamodb.DynamoDB, ddbTable string, partition, sort string, data map[string]string) error {
	return nil
}

// Delete xxx
func Delete(ddbConn *dynamodb.DynamoDB, ddbTable string, partition, sort string, data map[string]string) error {
	return nil
}

// Stream xxx
func Stream(ddbTable string, fromEnd bool, done chan string) (data chan map[string]string, events chan map[string]string, err error) {
	return nil, nil, nil
}

// ReadAll reads all data from the table and returns it. May run out of memory if reading an extremely large table. Also does not stream
// ongoing changes.
func ReadAll(ddbConn *dynamodb.DynamoDB, ddbTable string) ([]map[string]string, error) {
	rows := make([]map[string]string, 0, 1000)
	retry := 50

	var lastKey map[string]*dynamodb.AttributeValue

	keepReading := true
	for keepReading {
		keepReading = false

		scanInput := &dynamodb.ScanInput{
			TableName: aws.String(ddbTable),
		}
		if len(lastKey) == 0 {
			scanInput.ExclusiveStartKey = lastKey
		}
		if results, err := ddbConn.Scan(scanInput); err == nil {
			row := make(map[string]string)
			rows = append(rows, row)
			for _, item := range results.Items {
				for key, value := range item {
					row[key] = value.GoString()
				}
			}
			lastKey = results.LastEvaluatedKey
			if len(lastKey) > 0 {
				keepReading = true
			}
		} else {
			if aerr, ok := err.(awserr.Error); ok {
				switch aerr.Code() {
				case dynamodb.ErrCodeProvisionedThroughputExceededException:
					if retry >= 30*1000 {
						return nil, err
					}
					time.Sleep(time.Duration(retry) * time.Millisecond)
					retry *= 2
					keepReading = true
				default:
					return nil, err
				}
			} else {
				return nil, err
			}
		}
	}
	return rows, nil
}

// Read xxx
func Read(ddbConn *dynamodb.DynamoDB, ddbTable string) (chan map[string]string, chan bool, chan error) {
	items := make(chan map[string]string, 500)
	done  := make(chan bool, 1)
	err   := make(chan error)

	go func(items chan map[string]string, done chan bool, errChan chan error) {
		retry := 50

		var lastKey map[string]*dynamodb.AttributeValue

		keepReading := true
		for keepReading {
			keepReading = false

			scanInput := &dynamodb.ScanInput{
				TableName: aws.String(ddbTable),
			}
			if len(lastKey) == 0 {
				scanInput.ExclusiveStartKey = lastKey
			}
			if results, err := ddbConn.Scan(scanInput); err == nil {
				row := make(map[string]string)
				for _, item := range results.Items {
					for key, value := range item {
						row[key] = value.GoString()
					}
				}
				lastKey = results.LastEvaluatedKey
				if len(lastKey) > 0 {
					keepReading = true
				}
				items <- row
			} else {
				if aerr, ok := err.(awserr.Error); ok {
					switch aerr.Code() {
					case dynamodb.ErrCodeProvisionedThroughputExceededException:
						time.Sleep(time.Duration(retry) * time.Millisecond)
						retry *= 2
						keepReading = true
					default:
						errChan <- err
						return
					}
				} else {
					errChan <- err
					return
				}
			}
		}
		done <- true
	}(items, done, err)
	return items, done, err
}

// ReadStream reads a dynamo table and returns its content as a list of rows, each row a map - and also returns a channel of maps so that the caller
// can monitor ongoing changes to the table. This is done using dynamo streams.
// The caller can/should stop these background shard readers by sending a boolean value onto the stop channel (also returned to the caller).
func ReadStream(c *awssdk.Config, ddbTable string) ([]map[string]string, chan map[string]string, chan bool, error) {
	srlist := make(map[string]*ShardReader)    	// The list of shard readers. We'll build an initial set here, then the shard manager will maintain.
	srDone := make(chan string)                	// Give this to every shard reader. They'll use this to tell the shard manager when they've returned.
	items  := make(chan map[string]string, 100) // The channel of items we return to our caller. They will pull on this channel to see new stream items.
	stop   := make(chan bool, 1)                // Return this to the caller. Their way of telling us to stop our go routines.

	if ddb, err := Connect(c, nil); err == nil {
		if ddbDesc, err := ddb.DescribeTable(&dynamodb.DescribeTableInput{TableName: aws.String(ddbTable)}); err == nil {
			streamArn := *ddbDesc.Table.LatestStreamArn
			if session, err := awssdk.NewSession(c.Region, c.Key, c.Secret, c.Credfile, c.Profile); err == nil {
				ddbstrClient := dynamodbstreams.New(session)
				if strDesc, err := ddbstrClient.DescribeStream(&dynamodbstreams.DescribeStreamInput{StreamArn: aws.String(streamArn)}); err == nil {
					// Get the stream shards for this table. Need to do this first (before reading the table) because we need to first mark the
					// shard ends. After reading the table, we'll start reading the shards from the ends that we marked.
					for _, shard := range strDesc.StreamDescription.Shards {
						// If the shard has an ending sequence number, then no more records will be written to it. Skip it.
						if shard.SequenceNumberRange.EndingSequenceNumber == nil || *shard.SequenceNumberRange.EndingSequenceNumber == "" {
							sr := &ShardReader{shardID: *shard.ShardId, stop: make(chan bool)}
							// Get the iterators that mark the current end of the shards (streams) - *then* read the data - *then* start the shard readers.
							if shardIter, err := getShardIterator(ddbstrClient, streamArn, *shard.ShardId, dynamodbstreams.ShardIteratorTypeLatest, ""); err == nil {
								sr.shardIt = shardIter
							} else {
								return nil, nil, nil, err
							}
							srlist[sr.shardID] = sr
						}
					}
					// Read the table. We'll return this big chunk of data right away, along with a channel of items that we'll continually stream back over time.
					if tableData, err := ReadAll(ddb, ddbTable); err == nil {
						// Run the shard manager. It will look for new shards, and also remove shards that no longer exist on the stream.
						go runShardManager(ddbstrClient, streamArn, srlist, items, stop, srDone)

						// Run the shards. This is the shards that are there right now. The shard manager will add/remove shards to/from this list over time.
						for _, sr := range srlist {
							go sr.run(ddbstrClient, streamArn, items, srDone)
						}
						return tableData, items, stop, nil
					} else {
						return nil, nil, nil, err
					}
				} else {
					return nil, nil, nil, err
				}
			} else {
				return nil, nil, nil, err
			}
		} else {
			return nil, nil, nil, err
		}
	} else {
		return nil, nil, nil, err
	}
}

func runShardManager(ddbstrClient *dynamodbstreams.DynamoDBStreams, streamArn string, srlist map[string]*ShardReader, items chan map[string]string, stop chan bool, srDone chan string) {
	stopping := false
	for {
		select {
		case <-stop:
			stopping = true
			for _, sr := range srlist {
				sr.stop <- true
			}
		case <-time.After(time.Duration(5) * time.Second):
			if !stopping {
				if newStrDesc, err := ddbstrClient.DescribeStream(&dynamodbstreams.DescribeStreamInput{StreamArn: aws.String(streamArn)}); err == nil {
					for _, sr := range srlist {
						sr.inDDB = false
					}

					// Put AWS shards into a map for easier handling.
					awsShards := make(map[string]*dynamodbstreams.Shard)
					for _, awsShard := range newStrDesc.StreamDescription.Shards {
						awsShards[*awsShard.ShardId] = awsShard
					}

					// Look at our shards
					for shardID, sr := range srlist {
						// Does this shard still exist in AWS? (Probably yes)
						if awsShard, found := awsShards[shardID]; found {
							// If shard is closed, and we've read it all, remove it from our list (keep it if not done reading)
							if *awsShard.SequenceNumberRange.EndingSequenceNumber != "" {
								// The shard is closed. Did we read to the end of it? If so, it goes. Otherwise, keep it so we can read it to the end.
								if awsShard.SequenceNumberRange.EndingSequenceNumber != nil && sr.lastSeq == *awsShard.SequenceNumberRange.EndingSequenceNumber {
									// We got to the end of the shard. It's done.
									sr.stop <- true
									continue
								} else {
									// The shard is closed in AWS, but we haven't finished reading it yet. Finish, then remove it later.
								}
							} else {
								// The shard is still open in AWS.
							}
							// Is there an iterator? The shard runner should take care of getting new iterators, so nothing to check here.
						} else {
							// Our shard does not exist in AWS - remove it.
							sr.stop <- true
						}
					}
					// Find all shards in AWS that are not local, and add them.
					for awsShardID, awsShard := range awsShards {
						if _, found := srlist[awsShardID]; !found {
							// Only accept shards from AWS that are still open. If it's closed, then there's no new data there for us - nor will there be.
							if awsShard.SequenceNumberRange.EndingSequenceNumber == nil || *awsShard.SequenceNumberRange.EndingSequenceNumber == "" {
								// It's a new, open shard we haven't seen yet. Need to read from start.
								sr := &ShardReader{shardID: awsShardID, stop: make(chan bool), inDDB: true}
								srlist[sr.shardID] = sr
								go sr.run(ddbstrClient, streamArn, items, srDone)
							}
						}
					}
				}
			}
		case shardID := <-srDone:
			delete(srlist, shardID)
			if stopping && len(srlist) == 0 {
				return
			}
		}
	}
}

func (sr *ShardReader) run(client *dynamodbstreams.DynamoDBStreams, streamArn string, items chan map[string]string, done chan string) {
	// We could have an iterator already, if our caller already marked our starting point. If no iterator, get one starting from the beginning.
	// Note: we only need to get a LATEST iterator when getting the marker (which is always done in the caller prior to the one-time scan of the table).
	// LATEST - as the marker, prior to scanning the table (also after iterator expired, if no records read - no last sequence number)
	// AFTER_SEQUENCE_NUMBER - after expired iterator, need to pick up where we left off
	// TRIM_HORIZON - (the beginning) - when a new shard pops up (after we've scanned) - need all the data from this new shard
	if sr.shardIt == "" {
		if shardIter, err := getShardIterator(client, streamArn, sr.shardID, dynamodbstreams.ShardIteratorTypeTrimHorizon, ""); err == nil {
			sr.shardIt = shardIter
		} else {
			// Error trying to get an iterator for this new shard. Crap!
			done <-sr.shardID
			return
		}
	}
	retrying := false
	for {
		<- time.After(time.Duration(1) * time.Second)
		if getRecordOut, err := client.GetRecords(&dynamodbstreams.GetRecordsInput{ShardIterator: aws.String(sr.shardIt)}); err == nil {
			for _, record := range getRecordOut.Records {
				sr.lastSeq = *record.Dynamodb.SequenceNumber
				eventName := *record.EventName
				tableName := *record.EventSource
				item := make(map[string]string)
				item["_event"] = eventName
				item["_table"] = tableName
				var image map[string]*dynamodb.AttributeValue
				if eventName == "INSERT" || eventName == "MODIFY" {
					image = record.Dynamodb.NewImage
				} else {
					image = record.Dynamodb.OldImage
				}
				for attrName, attrVal := range image {
					item[attrName] = attrVal.GoString()
				}
				items <- item

				if getRecordOut.NextShardIterator != nil && *getRecordOut.NextShardIterator != "" {
					sr.shardIt = *getRecordOut.NextShardIterator
				}
			}
		} else {
			if aerr, ok := err.(awserr.Error); ok {
				switch aerr.Code() {
				case dynamodbstreams.ErrCodeResourceNotFoundException:
					// Non-existent stream. This shard reader is done.
					done <- sr.shardID
					return

				case dynamodbstreams.ErrCodeLimitExceededException:
					// Need to back off and retry.
					<-time.After(time.Duration(5) * time.Second)

				case dynamodbstreams.ErrCodeInternalServerError:
					// Not sure if retrying will help. Let's back off and retry once.
					if !retrying {
						retrying = true
						<-time.After(time.Duration(10) * time.Second)
					} else {
						done <- sr.shardID
						return
					}

				case dynamodbstreams.ErrCodeExpiredIteratorException:
					// Iterator expired. Shard still good. Get new iterator based on where we left off. (iterators expire after 15 minutes - according to doc)
					// If we have a lastSeq number, it means we have a starting point in the shard. If no lastSeq, we haven't read anything yet. Start at beginning.
					iterType := dynamodbstreams.ShardIteratorTypeAfterSequenceNumber
					if sr.lastSeq == "" {
						iterType = dynamodbstreams.ShardIteratorTypeTrimHorizon
					}
					if shardIter, err := getShardIterator(client, streamArn, sr.shardID, iterType, sr.lastSeq); err == nil {
						sr.shardIt = shardIter
					} else {
						done <- sr.shardID
						return
					}

				case dynamodbstreams.ErrCodeTrimmedDataAccessException:
					// Iterator goes back prior to 24 hours (or whatever the retention period is for this stream).
					// Get a new iterator for as far back as we can go - best we can do.
					if shardIter, err := getShardIterator(client, streamArn, sr.shardID, dynamodbstreams.ShardIteratorTypeTrimHorizon, ""); err == nil {
						sr.shardIt = shardIter
					} else {
						done <- sr.shardID
						return
					}

				default:
					// Should never get here.
					done <- sr.shardID
					return
				}
			}
		}
	}
}

func getShardIterator(client *dynamodbstreams.DynamoDBStreams, streamArn, shardID, iterType, seqNum string) (string, error) {
	if (iterType == dynamodbstreams.ShardIteratorTypeAtSequenceNumber || iterType == dynamodbstreams.ShardIteratorTypeAfterSequenceNumber) && seqNum == "" {
		return "", fmt.Errorf("Iterator type %s requires a sequence number", iterType)
	}
	input := dynamodbstreams.GetShardIteratorInput{
		ShardId:           aws.String(shardID),
		StreamArn:         aws.String(streamArn),
		ShardIteratorType: aws.String(iterType),
	}
	if iterType == dynamodbstreams.ShardIteratorTypeAtSequenceNumber || iterType == dynamodbstreams.ShardIteratorTypeAfterSequenceNumber {
		input.SequenceNumber = aws.String(seqNum)
	}
	if shardIterOut, err := client.GetShardIterator(&input); err != nil {
		return "", err
	} else {
		return *shardIterOut.ShardIterator, nil
	}
}
