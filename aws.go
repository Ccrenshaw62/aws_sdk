// Package awssdk is a simple package that provides access to AWS core functionality by wrapping the more complex AWS SDK.
package awssdk

import (
	"fmt"
	"os"
	"net"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
)

// Config xxx
type Config struct {
	Domain      string `yaml:"domain"`
	Region      string `yaml:"region"`
	Key         string `yaml:"key"`
	Secret      string `yaml:"secret"`
	Token       string `yaml:"token"`
	Credfile    string `yaml:"credfile"`
	Profile     string `yaml:"profile"`
}

// Credentials holds the various data values that will allow the caller to connect to AWS.
// Not all values need to be present. Should be either the CredFile (with an optional Profile), or
// the AwsKey and AwsSecret.
type Credentials struct {
	CredFile  string
	Profile   string
	AwsKey    string
	AwsSecret string
}

var defaultCredentials = Credentials{}

// DefaultCredentials xxx
func DefaultCredentials(file, profile, key, secret string) {
	defaultCredentials.CredFile = file
	defaultCredentials.Profile = profile
	defaultCredentials.AwsKey = key
	defaultCredentials.AwsSecret = secret
}

// NewSession will create and return a new AWS session object reference by attempting to connect to AWS using credentials.
// If userCred is nil, then the default credentials will be used. Note that those credentials would have had to have been
// set using the function DefaultCredentials(). 
// If authentication fails (or any other error occurs) the underlying error is returned to the caller and the session reference is nil.
func NewSession(region, key, secret, token, file, profile string) (*session.Session, error) {
	if key != "" && secret != "" {
		if sess, err := session.NewSession(&aws.Config{
			Region:      aws.String(region),
			Credentials: credentials.NewStaticCredentials(key, secret, token),
		}); err == nil {
			return sess, nil
		}
	}
	if file != "" {
		if sess, err := session.NewSession(&aws.Config{
			Region:      aws.String(region),
			Credentials: credentials.NewSharedCredentials(file, profile),
		}); err == nil {
			return sess, nil
		}
	}
	if sess, err := session.NewSession(&aws.Config{
		Region: aws.String(region),
	}); err == nil {
		return sess, nil
	} 
	return nil, fmt.Errorf("Cannot authenticate")
}

// MyIPv4Address returns a non-loopback IPv4 address on this host.
func MyIPv4Address() string {
	host, _ := os.Hostname()
	addrs, _ := net.LookupIP(host)
	for _, addr := range addrs {
		if ipv4 := addr.To4(); ipv4 != nil {
			return ipv4.String()
		}   
	}
	return "127.0.0.1"
}

func assertNotEmpty(val, msg string) {
	if val == "" {
		panic(msg)
	}
}
func assertNotNil(ptr interface{}, msg string) {
	fmt.Printf("assertNotNil(): test=%v msg=%s\n", ptr, msg)
	if ptr == nil {
		fmt.Printf("assertNotNil(): I should be panicking!!\n")
		panic(msg)
	}
}
func assertNil(ptr interface{}, msg string) {
	if ptr != nil {
		panic(msg)
	}
}
func assertTrue(yn bool, msg string) {
	if yn == false {
		panic(msg)
	}
}
func assertFalse(yn bool, msg string) {
	if yn {
		panic(msg)
	}
}
